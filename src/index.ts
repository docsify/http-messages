import {
  BadRequest,
  Forbidden,
  InternalServerError,
  NotFound,
  Unauthorized
} from 'http-errors';

import {
  IHttpErrorMessages,
  IMongooseError,
  IValidationErrorMessages
} from '../@types/index';

class Messages {
  public static HttpErrorMessages: IHttpErrorMessages = {
    badRequest: new BadRequest().message, // 400
    unauthorized: new Unauthorized().message, // 401
    forbidden: new Forbidden().message, // 403
    notFound: new NotFound().message, // 404
    internalServerError: new InternalServerError().message // 500
  };

  private validationErrorMessages: IValidationErrorMessages;

  constructor(validationErrorMessages?: IValidationErrorMessages) {
    this.validationErrorMessages = validationErrorMessages || {};
  }

  public getErrorMessage(err: any) {
    if (!err.isJoi || !err.details || !err.details[0]) {
      return err.name === 'ValidationError' && err._message
        ? err._message
        : err.message;
    }

    const details = err.details[0];

    return (
      this.validationErrorMessages[`${details.path}.${details.type}`] ||
      details.message ||
      err.message
    );
  }

  public parseMongooseErrors(error: any): IMongooseError {
    if (!error || !error.errors) {
      return null;
    }

    const result: IMongooseError = {
      name: error.name,
      message: error._message,
      errors: []
    };

    Object.keys(error.errors).forEach(key => {
      result.errors.push(error.errors[key].message);
    });

    return result;
  }
}

export = Messages;
