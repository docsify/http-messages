
declare class Messages {
  private validationErrorMessages: Messages.IValidationErrorMessages;

  public static HttpErrorMessages: Messages.IHttpErrorMessages;

  constructor(messages: Messages.IValidationErrorMessages);

  getErrorMessage(err: any): string;
  parseMongooseErrors(err: any): Messages.IMongooseError;
}

declare namespace Messages {
  interface IValidationErrorMessages {
    [index: string]: string;
  }

  interface IMongooseError {
    name: string;
    message: string;
    errors: string[];
  }

  interface IHttpErrorMessages {
    [index: string]: string;
  }

  interface IErrorMessages {
    [index: string]: string;
  }
  
  interface IMessages {
    [index: string]: string;
  }
}

export = Messages;
