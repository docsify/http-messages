"use strict";
const http_errors_1 = require("http-errors");
class Messages {
    constructor(validationErrorMessages) {
        this.validationErrorMessages = validationErrorMessages || {};
    }
    getErrorMessage(err) {
        if (!err.isJoi || !err.details || !err.details[0]) {
            return err.name === 'ValidationError' && err._message
                ? err._message
                : err.message;
        }
        const details = err.details[0];
        return (this.validationErrorMessages[`${details.path}.${details.type}`] ||
            details.message ||
            err.message);
    }
    parseMongooseErrors(error) {
        if (!error || !error.errors) {
            return null;
        }
        const result = {
            name: error.name,
            message: error._message,
            errors: []
        };
        Object.keys(error.errors).forEach(key => {
            result.errors.push(error.errors[key].message);
        });
        return result;
    }
}
Messages.HttpErrorMessages = {
    badRequest: new http_errors_1.BadRequest().message,
    unauthorized: new http_errors_1.Unauthorized().message,
    forbidden: new http_errors_1.Forbidden().message,
    notFound: new http_errors_1.NotFound().message,
    internalServerError: new http_errors_1.InternalServerError().message // 500
};
module.exports = Messages;
//# sourceMappingURL=index.js.map